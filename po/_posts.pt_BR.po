# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2017-06-20 07:46+1000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title #
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
#, no-wrap
msgid "\"F-Droid supports APK Expansion Files aka OBB\""
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid ""
"Many games, mapping, and other apps require a large amount of data to work.  "
"The APK file of an Android app is limited to 100MB in size, yet it is common "
"for a single country map file to be well over 100MB.  Also, in order to get "
"users running as quickly as possible, they should not have to wait for huge "
"amounts of data to download in order to just start the app for the first "
"time."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid ""
"Google created OBB aka \"[APK Expansion](https://developer.android.com/"
"google/play/expansion-files.html)\" files to provide a flexible means of "
"delivering large amounts of data.  This arragement also saves lots of "
"bandwidth since the APK file and the OBB file can be updated separately.  "
"For example, a game's assets do not need to change often, so they can be "
"shipped as an OBB. Then when the app itself is updated (i.e. the APK), it "
"does not need to include all those assets that are in the OBB file."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid ""
"OBB files are used by lots of apps like games and MAPS.ME.  F-Droid supports "
"OBB by downloading and installing the OBB before the APK, so that once the "
"APK is installed, the OBB files are already in place and ready to use.  F-"
"Droid also provides an `Intent` method for apps to fetch the OBB download "
"URLs in case the app itself needs to handle the OBB download/update.  That "
"is similar to how it works in Google Play."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid ""
"In order to use the OBB support, users need at least F-Droid v0.102, and the "
"repo must use _fdroidserver_ v0.7.0 or newer.  Adding OBB files to a repo is "
"very easy: just copy them to the same folder where the APKs go, i.e. _/path/"
"to/fdroid/repo/_."
msgstr ""

#. type: Title ##
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
#, no-wrap
msgid "Developer Usage"
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid ""
"One of the details about using OBB files in apps is that OBB files are not "
"guaranteed to be installed by the app store.  That means the app could "
"start, and the expected OBB files will not be there.  In that case, the app "
"must download and install the OBB file itself.  Google Play recommends using "
"their proprietary [Application Licensing](https://developer.android.com/"
"google/play/licensing/index.html)  service for this, F-Droid provides a "
"simple method that is all free software."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid ""
"To get the URL for the two possible OBB files, send an `Intent` to F-Droid "
"using these _Actions_:"
msgstr ""

#. type: Bullet: '* '
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid "`org.fdroid.fdroid.action.GET_OBB_MAIN_URL`"
msgstr ""

#. type: Bullet: '* '
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
msgid "`org.fdroid.fdroid.action.GET_OBB_PATCH_URL`"
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-f-droid-supports-apk-expansion-files-aka-obb.md
#, no-wrap
msgid ""
"Then download that URL using your favorite method, and make sure that\n"
"the file ultimately ends up in _Android/obb/<packageName>_ on the\n"
"device's External Storage.\n"
msgstr ""

#. type: Title #
#: build/_posts/md/2017-02-20-new-forum.md
#, no-wrap
msgid "\"Sailing into a new decade of discussions\""
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"We are excited to announce the release of the new official F-Droid forum: "
"[forum.f-droid.org](https://forum.f-droid.org)."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"We have been working hard to [revise the website](https://gitlab.com/groups/"
"fdroid/milestones/launch?title=Launch)  in order to make it easier to "
"modernize, accept contributions, and internationalise.  The main part of "
"this is switching the underlying technology from WordPress to [Jekyll]"
"(https://jekyllrb.com/).  However, this caused us to think about the forum "
"as it was powered by a WordPress plugin.  Quickly [we decided to drop the "
"old forum](https://gitlab.com/fdroid/fdroid-website/issues/6) in favor of an "
"indepentently hosted one and chose some modern software called [Discourse]"
"(https://www.discourse.org)."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"The old forum had a lot of disadvantages: Firstly, you cannot reply by "
"mail.  Secondly, the F-Droid volunteers had to waste time deleting massive "
"amounts of spam (the old forum had 25% spam).  Finally, the overall user "
"experience was bad, and not only because of increasing number of bugs found "
"in the forum software."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"With Discourse, we hope to breathe life back into the F-Droid community.  "
"While app submissions recently [moved to GitLab](https://gitlab.com/fdroid/"
"rfp), and bug reports/feature requests still live in GitLab, the new forum "
"is the new centre for all discussions related to F-Droid.  All users of F-"
"Droid, whether discussing the client or the server tools, can help each "
"other there."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"Because of temporary limitations of all F-Droid volunteers it was not "
"possible to move the old forum data to the new one which means that you have "
"to re-register yourself.  Independent from this all old topics remain at the "
"current URLs for now."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"One interesting new feature is [local communities](https://forum.f-droid.org/"
"c/other-languages).  In these communities you can discuss in your mother "
"tongue about all topics related to F-Droid and free software on Android.  At "
"the moment there is already a [German group](https://forum.f-droid.org/c/"
"other-languages/german)."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"If you want to create your own local community, please open a topic in the "
"[\"Other languages\" category](https://forum.f-droid.org/c/other-languages) "
"and we will discuss everything else there."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"Finally, there is one last insight into the current development of F-Droid: "
"we plan to use Discourse for [comments on the website](https://gitlab.com/"
"fdroid/fdroid-website/issues/36)  which gets us closer to [comments on "
"single applications](https://gitlab.com/fdroid/fdroidclient/issues/646)."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-02-20-new-forum.md
msgid ""
"Make sure you keep an eye on the new developments happening to F-Droid in "
"2017.  We for our part are curious if you like the new forum."
msgstr ""

#. type: Title #
#: build/_posts/md/2017-04-04-new-ux.md
#, no-wrap
msgid "\"A new UX 6 years in the making\""
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid ""
"F-Droid has been a part of the Android ecosystem for over 6 years now.  "
"Since then, over 2000 apps have been built for the main repository, many "
"[great features have been added](https://gitlab.com/fdroid/fdroidclient/blob/"
"master/CHANGELOG.md), the client has been translated into over 40 different "
"languages, and much more."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid ""
"However, the F-Droid UX has never changed much from [the original three tab "
"layout](https://f-droid.org/posts/f-droid-repository-alpha/):"
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
#, no-wrap
msgid ""
"{: .gallery}\n"
" * ![Version 0.11]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/0.11.png)\n"
" * ![Version 0.102]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/0.102.png)\n"
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid "This will change with the coming release of F-Droid client v0.103."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid ""
"Over the past 6 months, the F-Droid team has been busy redesigning a modern "
"UX which will help bring F-Droid into the present.  Some of the big ticket "
"items include:"
msgstr ""

#. type: Bullet: ' * '
#: build/_posts/md/2017-04-04-new-ux.md
msgid "Screenshots and feature graphics"
msgstr ""

#. type: Bullet: ' * '
#: build/_posts/md/2017-04-04-new-ux.md
msgid "Bulk download and install"
msgstr ""

#. type: Bullet: ' * '
#: build/_posts/md/2017-04-04-new-ux.md
msgid "Offline \"queue for download\""
msgstr ""

#. type: Bullet: ' * '
#: build/_posts/md/2017-04-04-new-ux.md
msgid "Ability to translate apps metadata"
msgstr ""

#. type: Bullet: ' * '
#: build/_posts/md/2017-04-04-new-ux.md
msgid "Improved notifications for downloads and pending updates"
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
#, no-wrap
msgid ""
"{: .gallery}\n"
" * ![\"Latest\" screen]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/latest.png)\n"
" * ![\"Categories\" screen]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/categories.png)\n"
" * ![\"Nearby\" screen]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/nearby.png)\n"
" * ![\"Updates\" screen]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/updates.png)\n"
" * ![App details screen (with screenshots)]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/appdetails-a.png)\n"
" * ![App details screen (without screenshots)]({{ site.baseurl }}/assets/posts/2017-04-04-new-ux/appdetails-b.png)\n"
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid ""
"In addition, we have been redesigning some of the smaller, but equally as "
"important parts of the app, such as better support for visually impaired "
"users who make use of tools such as Talkback, and better support for "
"donating to open source app developers."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid ""
"This has involved extensive design input from UX designers and the "
"community.  In addition to the user tests that were conducted early on in "
"the design process, further field tests are currently being conducted to "
"ensure that usability issues with the new design are identified and resolved."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid ""
"Over the coming weeks you will see alpha releases for v0.103.  We encourage "
"your feedback and suggestions about how to continually improve the F-Droid "
"client."
msgstr ""

#. type: Plain text
#: build/_posts/md/2017-04-04-new-ux.md
msgid ""
"This is one of the many improvements happening to the broader F-Droid "
"ecosystem in 2017, so keep an eye out for more!"
msgstr ""
